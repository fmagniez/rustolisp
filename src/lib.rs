pub use lisp::*;

mod lisp;

pub mod parser;

#[cfg(test)]
mod lisp_test {
    use std::fmt::Display;

    use crate::lisp::{Atom, EvalContext, Expr};
    use crate::parser::parse_lisp;

    #[test]
    fn test_display() {
        let nb: Atom = 1337.0.into();
        let list: Atom = vec![
            "cons".into(),
            42.0.into(),
            vec![
                nb.quote(),
                Atom::string("Albert"),
                vec![].into(),
            ].into(),
        ].into();
        let formatted = format!("{}", list);
        eprintln!("{}", formatted);
        assert_eq!(display_clean(formatted), "(CONS 42 ((QUOTE 1337) \"Albert\" NIL))")
    }

    fn display_clean<T: Display>(atom: T) -> String {
        lazy_static::lazy_static! {
            static ref COLOR_PATTERN: regex::Regex = regex::Regex::new("\x1B\\[(?:\\d+;(?:\\d+;)?)?\\d+m").unwrap();
        }
        COLOR_PATTERN
            .replace_all(format!("{}", atom).as_str(), "")
            .to_string()
    }

    fn assert_eval(context: &mut EvalContext, atom: &Atom, display: &str) {
        let result = atom.eval_map(context).unwrap();
        eprintln!("\x1B[37mCL-USER>\x1B[0m {}\n{}", atom, result);
        assert_eq!(display_clean(result), display);
    }

    fn assert_eval_err(context: &mut EvalContext, atom: &Atom, _display: &str) {
        let result = atom.eval_map(context).unwrap_err();
        // eprintln!("\x1B[37mCL-USER>\x1B[0m {}\n\x1B[1;31mERROR: {}", atom, result);
        eprintln!("\x1B[37mCL-USER>\x1B[0m {}\n\x1B[1;31mERREUR: {}", atom, result);
        // assert_eq!(display_clean(result), display); // FIXME Fix error message assertions
    }

    #[test]
    fn test_eval() {
        let mut context = EvalContext::new();

        let nb: Atom = 1337.0.into();

        assert_eval(&mut context, &nb, "1337");
        assert_eval(&mut context, &nb.quote(), "1337");
        assert_eval(&mut context, &vec![].into(), "NIL");
        assert_eval(&mut context, &nb.quote().quote(), "(QUOTE 1337)");
        assert_eval(&mut context, &Atom::from(vec![]).quote(), "NIL");
        assert_eval(&mut context, &Atom::from(vec![nb.clone(), "wut".into()]).quote(), "(1337 WUT)");
        assert_eval(&mut context, &Atom::from(vec![nb.clone()]).quote().quote(), "(QUOTE (1337))");
        assert_eval(&mut context, &vec!["quote".into(), nb.clone()].into(), "1337");
        assert_eval(&mut context, &vec!["+".into(), 1.0.into(), 2.0.into()].into(), "3");
        assert_eval(&mut context, &vec![
            "eval".into(),
            vec![
                "cdr".into(),
                Atom::from(vec![
                    "-".into(),
                    "+".into(),
                    1.0.into(),
                    2.0.into(),
                ]).quote(),
            ].into(),
        ].into(), "3");
        assert_eval(&mut context, &vec!["setq".into(), "x".into(), 17.0.into()].into(), "17");
        assert_eval(&mut context, &"x".into(), "17");
        assert_eval(&mut context, &vec!["if".into(), "x".into(), 1.0.into(), 2.0.into()].into(), "1");
        assert_eval(&mut context, &vec!["if".into(), vec![].into(), 1.0.into(), 2.0.into()].into(), "2");
        assert_eval(&mut context, &Atom::string("Albert"), "\"Albert\"");

        assert_eval_err(&mut context, &"a_var".into(), "no variable named A_VAR in scope\n  at eval A_VAR");
        assert_eval_err(&mut context, &vec!["a_fn".into()].into(), "no function named A_FN in scope\n  at eval (A_FN)");
        assert_eval_err(&mut context, &vec!["quote".into()].into(), "function QUOTE expected exactly 1 parameters, but 0 received\n  at eval (QUOTE)");
        assert_eval_err(&mut context, &vec![nb.clone()].into(), "1337 isn't a symbol\n  at eval (1337)");
    }

    fn assert_parse_eval(context: &mut EvalContext, input: &str, display: &str) -> Result<(), Box<dyn std::error::Error>> {
        let parsed = parse_lisp(input)?;
        let result = parsed.eval_map(context)?;
        eprintln!("\x1B[37mCL-USER>\x1B[0m {}\n{}", input, /*parsed,*/ result);
        assert_eq!(display_clean(result), display);
        Ok(())
    }

    fn assert_parse_eval_err(context: &mut EvalContext, input: &str, _display: &str) -> Result<(), Box<dyn std::error::Error>> {
        let parsed = parse_lisp(input)?;
        let result = parsed.eval_map(context).expect_err("error to be detected");
        // eprintln!("\x1B[37mCL-USER>\x1B[0m {}\n\x1B[1;31mERROR: {}\x1B[0m", input, /*parsed,*/ result);
        eprintln!("\x1B[37mCL-USER>\x1B[0m {}\n\x1B[1;31mERREUR: {}\x1B[0m", input, /*parsed,*/ result);
        // assert_eq!(display_clean(result), display); // FIXME Fix error message assertions
        Ok(())
    }

    fn assert_parse_err(input: &str, position: usize, _display: &str) {
        let result = parse_lisp(input).expect_err("syntax error to be detected");
        let pos = result.position();
        // eprintln!("\x1B[37mCL-USER>\x1B[0m {}\n         \x1B[1;31m{}\nSYNTAX ERROR: {}\x1B[0m", input, " ".repeat(pos) + "^", result);
        eprintln!("\x1B[37mCL-USER>\x1B[0m {}\n         \x1B[1;31m{}\nERREUR DE SYNTAXE: {}\x1B[0m", input, " ".repeat(pos) + "^", result);
        // assert_eq!(display_clean(result), display); // FIXME Fix error message assertions
        assert_eq!(pos, position);
    }

    #[test]
    fn test_parse() -> Result<(), Box<dyn std::error::Error>> {
        let mut context = EvalContext::new();

        assert_parse_eval(&mut context, "1337", "1337")?;
        assert_parse_eval(&mut context, "'1337", "1337")?;
        assert_parse_eval(&mut context, "''1337", "(QUOTE 1337)")?;

        assert_parse_eval(&mut context, "()", "NIL")?;
        assert_parse_eval(&mut context, "'()", "NIL")?;
        assert_parse_eval(&mut context, "'(1337)", "(1337)")?;
        assert_parse_eval(&mut context, "(+ 1337 (+ 42 13))", "1392")?;
        assert_parse_eval(&mut context, "(cdr '(wut 42 \"lol\"))", "(42 \"lol\")")?;

        assert_parse_eval(&mut context, " ( +  ( + 42 13 ) ' 1337 )", "1392")?;

        assert_parse_eval_err(&mut context, "(+ '(+ 42 13) 1337)", "(+ 42 13) is not a number\n  at validate (+ 42 13)\n  at eval (QUOTE (+ 42 13)) (parameter 1)\n  at eval (+ (QUOTE (+ 42 13)) 1337)")?;

        assert_parse_err(" ( +  ( + 42 13  ' 1337 )", 25, "missing closing parenthesis");
        // assert_parse_err(" ( +  ( + 42 13 ) ' 13.3.7 )", 20, "malformed number literal: 13.3.7");

        Ok(())
    }

    #[test]
    fn demo() -> Result<(), Box<dyn std::error::Error>> {
        let mut context = EvalContext::new();

        assert_parse_eval(&mut context, "1337", "1337")?;
        assert_parse_eval(&mut context, "'1337", "1337")?;
        assert_parse_eval(&mut context, "''1337", "(QUOTE 1337)")?;

        assert_parse_eval(&mut context, "()", "NIL")?;
        assert_parse_eval(&mut context, "'()", "NIL")?;
        assert_parse_eval(&mut context, "'(1337)", "(1337)")?;
        assert_parse_eval(&mut context, "(+ 1337 (+ 42 13))", "1392")?;
        assert_parse_eval(&mut context, "(EVAL (CADR (QUOTE (wut (+ 1 2)))))", "3")?;

        assert_parse_eval(&mut context, " ( +  ( + 42 13 ) ' 1337 )", "1392")?;

        assert_parse_eval_err(&mut context, "(+ '(+ 42 13) 1337)", "(+ 42 13) is not a number\n  at validate (+ 42 13)\n  at eval (QUOTE (+ 42 13)) (parameter 1)\n  at eval (+ (QUOTE (+ 42 13)) 1337)")?;

        assert_parse_eval_err(&mut context, "a_var", "no variable named A_VAR in scope\n  at eval A_VAR")?;
        assert_parse_eval_err(&mut context, "(a_fn)", "no function named A_FN in scope\n  at eval (A_FN)")?;
        assert_parse_eval_err(&mut context, "(quote)", "function QUOTE expected exactly 1 parameters, but 0 received\n  at eval (QUOTE)")?;
        assert_parse_eval_err(&mut context, "(1337)", "1337 isn't a symbol\n  at eval (1337)")?;

        assert_parse_eval_err(&mut context, "(setq x)", "function SETQ expected exactly 2 parameters, but 1 received\n  at eval (SETQ X)")?;
        assert_parse_eval_err(&mut context, "(setq 42 'wut)", "42 is not a symbol\n  at validate 42\n  at eval (SETQ 42 (QUOTE WUT))")?;

        assert_parse_err(" ( +  ( + 42 13  ' 1337 )", 25, "missing closing parenthesis");
        // assert_parse_err(" ( +  ( + 42 13 ) ' 13.3.7 )", 20, "malformed number literal: 13.3.7");

        Ok(())
    }

    #[test]
    fn ia_td1_ex1() -> Result<(), Box<dyn std::error::Error>> {
        let mut context = EvalContext::new();

        assert_parse_eval(&mut context, "23", "23")?;
        assert_parse_eval(&mut context, "(quote 23)", "23")?;
        assert_parse_eval(&mut context, "'23", "23")?;
        assert_parse_eval_err(&mut context, "(set x 32)", "no variable named X in scope\n  at eval X (parameter 1)\n  at eval (SET X 32)")?;
        assert_parse_eval(&mut context, "(setq x 32)", "32")?;
        assert_parse_eval(&mut context, "(list x x 32)", "(32 32 32)")?;
        assert_parse_eval(&mut context, "(cadr (list x x 32))", "32")?;
        assert_parse_eval(&mut context, "(setq x 'y)", "Y")?;
        assert_parse_eval(&mut context, "(setq xx 5)", "5")?;
        assert_parse_eval(&mut context, "(setq y '(+ xx xx 32))", "(+ XX XX 32)")?;
        assert_parse_eval(&mut context, "x", "Y")?;
        assert_parse_eval(&mut context, "(eval x)", "(+ XX XX 32)")?;
        assert_parse_eval(&mut context, "(eval y)", "42")?;
        assert_parse_eval(&mut context, "(cadr y)", "XX")?;
        assert_parse_eval_err(&mut context, "(eval (list '+ x (cadr y)))", "47")?;
        assert_parse_eval_err(&mut context, "(setq z (+ (if x 2 0) (caddr y) (* y y)))", "XX is not a number\n  at validate XX\n  at eval (CADDR Y) (parameter 2)\n  at eval (+ (IF X 2 0) (CADDR Y) (* Y Y)) (parameter 2)\n  at eval (SETQ Z (+ (IF X 2 0) (CADDR Y) (* Y Y)))")?;
        assert_parse_eval(&mut context, "(setq y (list (setq z \"Albert\") x y))", "(\"Albert\" Y (+ XX XX 32))")?;
        assert_parse_eval(&mut context, "z", "\"Albert\"")?;
        assert_parse_eval(&mut context, "y", "(\"Albert\" Y (+ XX XX 32))")?;
        assert_parse_eval_err(&mut context, "(setq x (* x x))", "Y is not a number\n  at validate Y\n  at eval X (parameter 1)\n  at eval (* X X) (parameter 2)\n  at eval (SETQ X (* X X))")?;
        assert_parse_eval(&mut context, "x", "Y")?;

        Ok(())
    }
}
