pub use super::super::*;

#[derive(Clone, Debug)]
pub struct LispList(pub Option<(Atom, Arc<LispList>)>);

impl LispList {
    pub fn cons(&self) -> Option<&Atom> {
        self.0
            .as_ref()
            .map(|(v, _)| v)
    }
}

pub struct LispListIterator {
    list: Arc<LispList>,
}

impl Iterator for LispListIterator {
    type Item = Atom;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((v, next)) = self.list.0.as_ref() {
            let v = v.clone();
            self.list = next.clone();
            Some(v)
        } else {
            None
        }
    }
}

impl From<Vec<Atom>> for LispList {
    fn from(items: Vec<Atom>) -> Self {
        fn from_vec<T: Into<Atom>>(mut iter: impl Iterator<Item=T>) -> LispList {
            let v = iter.next();
            if let Some(v) = v {
                LispList(Some((v.into(), Arc::new(from_vec(iter)))))
            } else {
                LispList(None)
            }
        }
        from_vec(items.into_iter())
    }
}

impl IntoIterator for LispList {
    type Item = Atom;
    type IntoIter = LispListIterator;

    fn into_iter(self) -> Self::IntoIter {
        LispListIterator {
            list: Arc::new(self)
        }
    }
}

impl Display for LispList {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "(")?;
        if let Some((v, next)) = self.0.as_ref() {
            if let Atom::Symbol(v) = v {
                write!(f, "\x1B[1m{}", v)?;
            } else {
                write!(f, "{}", v)?;
            }
            let mut next = next;
            while let Some((v, n)) = (**next).0.as_ref() {
                write!(f, " {}", v)?;
                next = n;
            }
        }
        write!(f, ")")
    }
}

impl Expr for LispList {
    fn eval(&self, context: &mut EvalContext) -> Result<Atom> {
        if let Some((v, next)) = self.0.as_ref() {
            context.call(
                v.as_symbol()
                    .map_err(|_| LispError::DataListNotEvaluable {
                        list: Arc::new(self.clone())
                    })?,
                next.deref().clone().into_iter().collect(),
            )
        } else {
            Ok("NIL".into())
        }
    }

    fn is_nil(&self) -> bool {
        self.0.is_none()
    }

    fn as_list(&self) -> Result<&LispList> {
        Ok(self)
    }
}
