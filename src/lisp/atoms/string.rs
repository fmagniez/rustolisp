pub use super::super::*;

#[derive(Clone, Debug)]
pub struct LispString(pub String);

impl Display for LispString {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "\x1B[1;32m\"{}\"\x1B[0m", self.0.replace('"', "\\\""))
    }
}

impl Expr for LispString {
    fn eval(&self, _: &mut EvalContext) -> Result<Atom> {
        Ok(self.clone().into())
    }

    fn as_string(&self) -> Result<&LispString> {
        Ok(self)
    }
}
