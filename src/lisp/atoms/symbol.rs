pub use super::super::*;

#[derive(Clone, Debug)]
pub struct LispSymbol(pub String);

impl Display for LispSymbol {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "\x1B[33m{}\x1B[0m", self.0)
    }
}

impl Expr for LispSymbol {
    fn eval(&self, context: &mut EvalContext) -> Result<Atom> {
        context.get(self)
    }

    fn is_nil(&self) -> bool {
        self.0 == "NIL"
    }

    fn as_symbol(&self) -> Result<&LispSymbol> {
        Ok(self)
    }
}
