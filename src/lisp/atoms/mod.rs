pub use list::*;
pub use number::*;
pub use string::*;
pub use symbol::*;

mod symbol;
mod number;
mod string;
mod list;

#[derive(Clone, Debug)]
pub enum Atom {
    Symbol(Arc<LispSymbol>),
    Number(Arc<LispNumber>),
    String(Arc<LispString>),
    List(Arc<LispList>),
}

impl Atom {
    pub fn symbol(name: &str) -> Atom {
        Atom::Symbol(Arc::new(LispSymbol(name.to_uppercase())))
    }
    pub fn number<T: Into<f64>>(value: T) -> Atom {
        Atom::Number(Arc::new(LispNumber(value.into())))
    }
    pub fn string(value: &str) -> Atom {
        Atom::String(Arc::new(LispString(value.to_string())))
    }
    pub fn list<T: Into<Atom>>(items: Vec<T>) -> Atom {
        fn from_vec<T: Into<Atom>>(mut iter: impl Iterator<Item=T>) -> Arc<LispList> {
            let v = iter.next();
            Arc::new(if let Some(v) = v {
                LispList(Some((v.into(), from_vec(iter))))
            } else {
                LispList(None)
            })
        }
        if items.is_empty() {
            Atom::symbol("NIL")
        } else {
            Atom::List(from_vec(items.into_iter()))
        }
    }

    pub fn quote(&self) -> Atom {
        vec!["quote".into(), self.clone()].into()
    }
}

impl From<LispSymbol> for Atom {
    fn from(v: LispSymbol) -> Self {
        Self::Symbol(Arc::new(v))
    }
}

impl From<LispNumber> for Atom {
    fn from(v: LispNumber) -> Self {
        Self::Number(Arc::new(v))
    }
}

impl From<LispString> for Atom {
    fn from(v: LispString) -> Self {
        Self::String(Arc::new(v))
    }
}

impl From<LispList> for Atom {
    fn from(v: LispList) -> Self {
        if v.is_nil() {
            Self::symbol("NIL")
        } else {
            Self::List(Arc::new(v))
        }
    }
}

impl From<&str> for Atom {
    fn from(sym: &str) -> Self {
        Self::symbol(sym)
    }
}

impl From<f64> for Atom {
    fn from(nb: f64) -> Self {
        Self::number(nb)
    }
}

impl From<Vec<Atom>> for Atom {
    fn from(lst: Vec<Atom>) -> Self {
        Self::list(lst)
    }
}

impl Display for Atom {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Atom::Symbol(v) => Display::fmt(v, f),
            Atom::Number(v) => Display::fmt(v, f),
            Atom::String(v) => Display::fmt(v, f),
            Atom::List(v) => Display::fmt(v, f),
        }
    }
}

impl Expr for Atom {
    fn eval(&self, context: &mut EvalContext) -> Result<Atom> {
        match self {
            Atom::Symbol(v) => Expr::eval(v.deref(), context),
            Atom::Number(v) => Expr::eval(v.deref(), context),
            Atom::String(v) => Expr::eval(v.deref(), context),
            Atom::List(v) => Expr::eval(v.deref(), context),
        }
    }

    fn is_nil(&self) -> bool {
        match self {
            Atom::Symbol(v) => Expr::is_nil(v.deref()),
            Atom::Number(v) => Expr::is_nil(v.deref()),
            Atom::String(v) => Expr::is_nil(v.deref()),
            Atom::List(v) => Expr::is_nil(v.deref()),
        }
    }

    fn as_symbol(&self) -> Result<&LispSymbol> {
        match self {
            Atom::Symbol(v) => Expr::as_symbol(v.deref()),
            Atom::Number(v) => Expr::as_symbol(v.deref()),
            Atom::String(v) => Expr::as_symbol(v.deref()),
            Atom::List(v) => Expr::as_symbol(v.deref()),
        }
    }

    fn as_number(&self) -> Result<&LispNumber> {
        match self {
            Atom::Symbol(v) => Expr::as_number(v.deref()),
            Atom::Number(v) => Expr::as_number(v.deref()),
            Atom::String(v) => Expr::as_number(v.deref()),
            Atom::List(v) => Expr::as_number(v.deref()),
        }
    }

    fn as_string(&self) -> Result<&LispString> {
        match self {
            Atom::Symbol(v) => Expr::as_string(v.deref()),
            Atom::Number(v) => Expr::as_string(v.deref()),
            Atom::String(v) => Expr::as_string(v.deref()),
            Atom::List(v) => Expr::as_string(v.deref()),
        }
    }

    fn as_list(&self) -> Result<&LispList> {
        match self {
            Atom::Symbol(v) => Expr::as_list(v.deref()),
            Atom::Number(v) => Expr::as_list(v.deref()),
            Atom::String(v) => Expr::as_list(v.deref()),
            Atom::List(v) => Expr::as_list(v.deref()),
        }
    }
}
