pub use super::super::*;

#[derive(Clone, Debug)]
pub struct LispNumber(pub f64);

impl Display for LispNumber {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "\x1B[1;36m{}\x1B[0m", self.0)
    }
}

impl Expr for LispNumber {
    fn eval(&self, _: &mut EvalContext) -> Result<Atom> {
        Ok(self.clone().into())
    }

    fn as_number(&self) -> Result<&LispNumber> {
        Ok(self)
    }
}
