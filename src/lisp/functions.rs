use super::*;

pub trait FromAtom: Sized {
    fn parse(context: &mut EvalContext, atom: Atom) -> Result<Self>;
}

impl FromAtom for LispSymbol {
    fn parse(_: &mut EvalContext, value: Atom) -> Result<Self> {
        if let Atom::Symbol(nb) = value {
            Ok((*nb).clone())
        } else {
            Err(LispError::NotASymbol {
                value: value.clone()
            }).map_err(|e| LispError::ValidateError {
                atom: value,
                err: Box::new(e),
            })
        }
    }
}

impl FromAtom for LispNumber {
    fn parse(_: &mut EvalContext, value: Atom) -> Result<Self> {
        if let Atom::Number(nb) = value {
            Ok((*nb).clone())
        } else {
            Err(LispError::NotANumber {
                value: value.clone()
            }).map_err(|e| LispError::ValidateError {
                atom: value,
                err: Box::new(e),
            })
        }
    }
}

impl FromAtom for LispString {
    fn parse(_: &mut EvalContext, value: Atom) -> Result<Self> {
        if let Atom::String(nb) = value {
            Ok((*nb).clone())
        } else {
            Err(LispError::NotAString {
                value: value.clone()
            }).map_err(|e| LispError::ValidateError {
                atom: value,
                err: Box::new(e),
            })
        }
    }
}

impl FromAtom for LispList {
    fn parse(_: &mut EvalContext, value: Atom) -> Result<Self> {
        if let Atom::List(list) = value {
            Ok((*list).clone())
        } else {
            if let Atom::Symbol(maybe_nil) = &value {
                if maybe_nil.0 == "NIL" {
                    return Ok(LispList(None));
                }
            }
            Err(LispError::NotAList {
                value: value.clone()
            }).map_err(|e| LispError::ValidateError {
                atom: value,
                err: Box::new(e),
            })
        }
    }
}

impl FromAtom for Atom {
    fn parse(_: &mut EvalContext, atom: Atom) -> Result<Self> {
        Ok(atom)
    }
}

pub trait FromLispArgument: Sized {
    fn from(context: &mut EvalContext, name: &LispSymbol, params: Vec<Atom>) -> Result<Self>;
}

pub struct Eval<T: FromAtom> {
    inner: T,
}

impl<T: FromAtom> Eval<T> {
    pub fn unwrap(self) -> T {
        self.inner
    }
}

impl<T: FromAtom> Deref for Eval<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T: FromAtom> DerefMut for Eval<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl<T: FromAtom> FromAtom for Eval<T> {
    fn parse(context: &mut EvalContext, atom: Atom) -> Result<Self> {
        let evaluated = atom.eval_map(context)?;
        Ok(Eval {
            inner: T::parse(context, evaluated)
                .map_err(|e| LispError::EvalError {
                    atom,
                    err: Box::new(e),
                })?
        })
    }
}

impl<T: FromAtom> FromLispArgument for Vec<T> {
    fn from(context: &mut EvalContext, name: &LispSymbol, params: Vec<Atom>) -> Result<Self> {
        let function: LispList = {
            let mut params = params.clone();
            params.insert(0, name.clone().into());
            params.into()
        };
        params
            .into_iter()
            .enumerate()
            .map(|(i, atom)|
                T::parse(context, atom)
                    .map_err(|e| if let LispError::EvalError { atom, err } = e {
                        LispError::EvalParameter {
                            atom,
                            parameter: i + 1,
                            err,
                        }
                    } else {
                        LispError::InvalidParameter {
                            function: function.clone(),
                            parameter: i + 1,
                            err: Box::new(e),
                        }
                    })
            )
            .collect()
    }
}

impl<T: FromAtom> FromLispArgument for T {
    fn from(context: &mut EvalContext, name: &LispSymbol, params: Vec<Atom>) -> Result<Self> {
        let function = {
            let mut params = params.clone();
            params.insert(0, name.clone().into());
            params.into()
        };
        if params.len() == 1 {
            let mut iter = params.into_iter();
            Ok(T::parse(context, iter.next().unwrap())
                .map_err(|e| if let LispError::EvalError { atom, err } = e {
                    LispError::EvalParameter {
                        atom,
                        parameter: 1,
                        err,
                    }
                } else {
                    LispError::InvalidParameter {
                        function,
                        parameter: 1,
                        err: Box::new(e),
                    }
                })?)
        } else {
            Err(LispError::ExactNumberOfParametersExpected {
                function: name.clone(),
                expected: 1,
                found: params.len(),
            })
        }
    }
}

impl<T: FromAtom, U: FromAtom> FromLispArgument for (T, U) {
    fn from(context: &mut EvalContext, name: &LispSymbol, params: Vec<Atom>) -> Result<Self> {
        let function: LispList = {
            let mut params = params.clone();
            params.insert(0, name.clone().into());
            params.into()
        };
        if params.len() == 2 {
            let mut iter = params.into_iter();
            Ok((
                T::parse(context, iter.next().unwrap())
                    .map_err(|e| if let LispError::EvalError { atom, err } = e {
                        LispError::EvalParameter {
                            atom,
                            parameter: 1,
                            err,
                        }
                    } else {
                        LispError::InvalidParameter {
                            function: function.clone(),
                            parameter: 1,
                            err: Box::new(e),
                        }
                    })?,
                U::parse(context, iter.next().unwrap())
                    .map_err(|e| if let LispError::EvalError { atom, err } = e {
                        LispError::EvalParameter {
                            atom,
                            parameter: 2,
                            err,
                        }
                    } else {
                        LispError::InvalidParameter {
                            function: function.clone(),
                            parameter: 2,
                            err: Box::new(e),
                        }
                    })?
            ))
        } else {
            Err(LispError::ExactNumberOfParametersExpected {
                function: name.clone(),
                expected: 2,
                found: params.len(),
            })
        }
    }
}

impl<T: FromAtom, U: FromAtom, V: FromAtom> FromLispArgument for (T, U, V) {
    fn from(context: &mut EvalContext, name: &LispSymbol, params: Vec<Atom>) -> Result<Self> {
        let function: LispList = {
            let mut params = params.clone();
            params.insert(0, name.clone().into());
            params.into()
        };
        if params.len() == 3 {
            let mut iter = params.into_iter();
            Ok((
                T::parse(context, iter.next().unwrap())
                    .map_err(|e| if let LispError::EvalError { atom, err } = e {
                        LispError::EvalParameter {
                            atom,
                            parameter: 1,
                            err,
                        }
                    } else {
                        LispError::InvalidParameter {
                            function: function.clone(),
                            parameter: 1,
                            err: Box::new(e),
                        }
                    })?,
                U::parse(context, iter.next().unwrap())
                    .map_err(|e| if let LispError::EvalError { atom, err } = e {
                        LispError::EvalParameter {
                            atom,
                            parameter: 2,
                            err,
                        }
                    } else {
                        LispError::InvalidParameter {
                            function: function.clone(),
                            parameter: 2,
                            err: Box::new(e),
                        }
                    })?,
                V::parse(context, iter.next().unwrap())
                    .map_err(|e| if let LispError::EvalError { atom, err } = e {
                        LispError::EvalParameter {
                            atom,
                            parameter: 3,
                            err,
                        }
                    } else {
                        LispError::InvalidParameter {
                            function: function.clone(),
                            parameter: 3,
                            err: Box::new(e),
                        }
                    })?
            ))
        } else {
            Err(LispError::ExactNumberOfParametersExpected {
                function: name.clone(),
                expected: 3,
                found: params.len(),
            })
        }
    }
}

impl<T: FromAtom, U: FromAtom> FromLispArgument for (T, Vec<U>) {
    fn from(context: &mut EvalContext, name: &LispSymbol, params: Vec<Atom>) -> Result<Self> {
        let function: LispList = {
            let mut params = params.clone();
            params.insert(0, name.clone().into());
            params.into()
        };
        if params.len() >= 1 {
            let mut iter = params.into_iter();
            Ok((
                T::parse(context, iter.next().unwrap())
                    .map_err(|e| if let LispError::EvalError { atom, err } = e {
                        LispError::EvalParameter {
                            atom,
                            parameter: 1,
                            err,
                        }
                    } else {
                        LispError::InvalidParameter {
                            function: function.clone(),
                            parameter: 1,
                            err: Box::new(e),
                        }
                    })?,
                iter
                    .enumerate()
                    .map(|(i, atom)|
                        U::parse(context, atom)
                            .map_err(|e| if let LispError::EvalError { atom, err } = e {
                                LispError::EvalParameter {
                                    atom,
                                    parameter: i + 2,
                                    err,
                                }
                            } else {
                                LispError::InvalidParameter {
                                    function: function.clone(),
                                    parameter: i + 2,
                                    err: Box::new(e),
                                }
                            })
                    )
                    .collect::<Result<Vec<_>>>()?
            ))
        } else {
            Err(LispError::AtLeastParametersExpected {
                function: name.clone(),
                expected: 1,
                found: params.len(),
            })
        }
    }
}
