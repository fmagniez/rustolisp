use super::*;

#[derive(thiserror::Error, Debug)]
pub enum LispError {
    #[error("\x1B[0m{}\x1B[1;31m n'est pas un Symbole", value)]
    NotASymbol { value: Atom },
    #[error("\x1B[0m{}\x1B[1;31m n'est pas un Nombre", value)]
    NotANumber { value: Atom },
    #[error("\x1B[0m{}\x1B[1;31m n'est pas une Chaine de caractères", value)]
    NotAString { value: Atom },
    #[error("\x1B[0m{}\x1B[1;31m n'est pas une Liste", value)]
    NotAList { value: Atom },

    #[error("Variable \x1B[0m{}\x1B[1;31m non définie", symbol)]
    VariableNotInScope { symbol: LispSymbol },
    #[error("Fonction \x1B[0m{}\x1B[1;31m non définie", symbol)]
    FunctionNotInScope { symbol: LispSymbol },
    #[error("\x1B[0m{}\x1B[1;31m est un symbole défini par LISP, il ne peut être modifié", symbol)]
    LanguageDefinedSymbol { symbol: LispSymbol },
    #[error("\x1B[0m{}\x1B[1;31m n'est pas un Symbole\n  NOTE: Le premier élément d'une liste à évaluer doit être un Symbole représentant la fonction à appeler", list.cons().unwrap())]
    DataListNotEvaluable { list: Arc<LispList> },
    #[error("{}\n    lors de l'évaluation de \x1B[0m{}\x1B[1;31m", err, atom)]
    EvalError { atom: Atom, err: Box<LispError> },
    #[error("{}\n    lors de la vérification du type de \x1B[0m{}\x1B[1;31m", err, atom)]
    ValidateError { atom: Atom, err: Box<LispError> },
    #[error("{}", err)]
    InvalidParameter { function: LispList, parameter: usize, err: Box<LispError> },
    #[error("{}\n    lors de l'évaluation de \x1B[0m{}\x1B[1;31m (élément n°{} de la liste ci-dessous)", err, atom, parameter + 1)]
    EvalParameter { atom: Atom, parameter: usize, err: Box<LispError> },
    #[error("{}\n    lors de l'évaluation de \x1B[0m{}\x1B[1;31m (via la fonction eval)", err, atom)]
    EvalFunction { atom: Atom, err: Box<LispError> },
    #[error("La fonction \x1B[0m{}\x1B[1;31m attendait exactement \x1B[4m{}\x1B[0;1;31m paramètre(s) mais \x1B[4m{}\x1B[0;1;31m ont été fourni", function, expected, found)]
    ExactNumberOfParametersExpected { function: LispSymbol, expected: usize, found: usize },
    #[error("La fonction \x1B[0m{}\x1B[1;31m attendait au moins \x1B[4m{}\x1B[0;1;31m paramètre(s) mais \x1B[4m{}\x1B[0;1;31m ont été fourni", function, expected, found)]
    AtLeastParametersExpected { function: LispSymbol, expected: usize, found: usize },

    #[error("Division par zéro: \x1B[1;36m{}\x1B[0m / \x1B[1;36m{}\x1B[0;1;31m", first, second)]
    DivisionByZero { first: f64, second: f64 },
}

// VERSION ANGLAISE

// #[derive(thiserror::Error, Debug)]
// pub enum LispError {
//     // #[error("expected a symbol but got \x1B[0m{}\x1B[1;31m", value)]
//     #[error("\x1B[0m{}\x1B[1;31m is not a symbol", value)]
//     NotASymbol { value: Atom },
//     // #[error("expected a number but got \x1B[0m{}\x1B[1;31m", value)]
//     #[error("\x1B[0m{}\x1B[1;31m is not a number", value)]
//     NotANumber { value: Atom },
//     // #[error("expected a string but got \x1B[0m{}\x1B[1;31m", value)]
//     #[error("\x1B[0m{}\x1B[1;31m is not a string", value)]
//     NotAString { value: Atom },
//     // #[error("expected a list but got \x1B[0m{}\x1B[1;31m", value)]
//     #[error("\x1B[0m{}\x1B[1;31m is not a list", value)]
//     NotAList { value: Atom },
//
//     #[error("no variable named \x1B[0m{}\x1B[1;31m in scope", symbol)]
//     VariableNotInScope { symbol: LispSymbol },
//     #[error("no function named \x1B[0m{}\x1B[1;31m in scope", symbol)]
//     FunctionNotInScope { symbol: LispSymbol },
//     #[error("\x1B[0m{}\x1B[1;31m is a language-defined symbol, it cannot be updated", symbol)]
//     LanguageDefinedSymbol { symbol: LispSymbol },
//     // #[error("\x1B[0m{}\x1B[1;31m cannot be evaluated because \x1B[0m{}\x1B[1;31m isn't a symbol", list, list.cons().unwrap())]
//     // #[error("\x1B[0m{}\x1B[1;31m isn't a symbol (the first element of the list should have been a function symbol)", list.cons().unwrap())]
//     #[error("\x1B[0m{}\x1B[1;31m isn't a symbol", list.cons().unwrap())]
//     DataListNotEvaluable { list: Arc<LispList> },
//     // #[error("call to function \x1B[0m{}\x1B[1;31m: invalid parameter \x1B[4m{}\x1B[0;1;31m: {}", function, parameter, err)]
//     #[error("{}\n  at eval \x1B[0m{}\x1B[1;31m", err, atom)]
//     EvalError { atom: Atom, err: Box<LispError> },
//     #[error("{}\n  at validate \x1B[0m{}\x1B[1;31m", err, atom)]
//     ValidateError { atom: Atom, err: Box<LispError> },
//     // #[error("{}\n  at parameter \x1B[4m{}\x1B[0;1;31m in function call \x1B[0m{}\x1B[1;31m", err, parameter, function)]
//     #[error("{}", err)]
//     InvalidParameter { function: LispList, parameter: usize, err: Box<LispError> },
//     #[error("{}\n  at eval \x1B[0m{}\x1B[1;31m (parameter {})", err, atom, parameter)]
//     EvalParameter { atom: Atom, parameter: usize, err: Box<LispError> },
//     #[error("{}\n  at eval \x1B[0m{}\x1B[1;31m (eval function)", err, atom)]
//     EvalFunction { atom: Atom, err: Box<LispError> },
//     #[error("function \x1B[0m{}\x1B[1;31m expected exactly \x1B[4m{}\x1B[0;1;31m parameters, but \x1B[4m{}\x1B[0;1;31m received", function, expected, found)]
//     ExactNumberOfParametersExpected { function: LispSymbol, expected: usize, found: usize },
//     #[error("function {} expected at least {} parameters, but {} received", function, expected, found)]
//     AtLeastParametersExpected { function: String, expected: usize, found: usize },
// }
