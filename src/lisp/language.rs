use super::*;
use super::functions::*;

impl EvalContext {
    pub(crate) fn call(&mut self, name: &LispSymbol, params: Vec<Atom>) -> Result<Atom> {
        let fn_name = name.0.as_str();
        match fn_name {
            "QUOTE" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.quote(params)
            }
            "LIST" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.list(params)
            }
            "CONS" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.cons(params)
            }
            "EVAL" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.eval(params)
            }
            "SET" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.set_lisp(params)
            }
            "SETQ" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.set_lisp_quote(params)
            }
            "IF" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.if_lisp(params)
            }

            "+" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.add(params)
            }
            "-" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.sub(params)
            }
            "*" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.mul(params)
            }
            "/" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.div(params)
            }
            "MOD" => {
                let params = FromLispArgument::from(self, name, params)?;
                self.modulus(params)
            }

            _ => {
                lazy_static::lazy_static! {
                        static ref CADR: regex::Regex = regex::Regex::new(r#"^C(?:AD*|D+)R"#).unwrap();
                    }
                if CADR.is_match(fn_name) {
                    let params = FromLispArgument::from(self, name, params)?;
                    return self.cadr(fn_name, params);
                }
                Err(LispError::FunctionNotInScope { symbol: name.clone() })
            }
        }
    }

    fn quote(&mut self, param: Atom) -> Result<Atom> {
        Ok(param)
    }

    fn cons(&mut self, (first, list): (Eval<Atom>, Eval<LispList>)) -> Result<Atom> {
        Ok(
            <Atom as From<LispList>>::from(LispList(Some((first.unwrap(), Arc::new(list.unwrap())))))
        )
    }

    fn list(&mut self, items: Vec<Eval<Atom>>) -> Result<Atom> {
        Ok(
            items
                .into_iter()
                .map(Eval::unwrap)
                .collect::<Vec<_>>()
                .into()
        )
    }

    fn eval(&mut self, expr: Atom) -> Result<Atom> {
        let first = expr.eval_map(self)?;
        let second = first.eval_map(self);
        second // eval function
            .map_err(|e| LispError::EvalParameter {
                atom: expr,
                parameter: 1,
                err: if let LispError::EvalError { atom, err } = e {
                    Box::new(LispError::EvalFunction {
                        atom,
                        err,
                    })
                } else {
                    Box::new(e)
                },
            })
    }

    fn set_lisp(&mut self, (name, value): (Eval<LispSymbol>, Eval<Atom>)) -> Result<Atom> {
        self.set(&name, value.clone())?;
        Ok(value.unwrap())
    }

    fn set_lisp_quote(&mut self, (name, value): (LispSymbol, Eval<Atom>)) -> Result<Atom> {
        self.set(&name, value.clone())?;
        Ok(value.unwrap())
    }

    fn cadr(&mut self, name: &str, list: Eval<LispList>) -> Result<Atom> {
        let mut list = &*list;
        let is_car = name.contains('A');
        let mut depth = name
            .chars()
            .filter(|c| *c == 'D')
            .count();
        while depth > 0 {
            depth -= 1;
            if let Some((_, next)) = list.0.as_ref() {
                list = next;
            } else {
                break;
            }
        }
        Ok(if !is_car {
            list.clone().into()
        } else if let Some((v, _)) = list.0.as_ref() {
            v.clone()
        } else {
            vec![].into()
        })
    }

    fn if_lisp(&mut self, (cond, on_true, on_false): (Eval<Atom>, Atom, Atom)) -> Result<Atom> {
        (if !cond.is_nil() {
            &on_true
        } else {
            &on_false
        }).eval_map(self)
    }

    fn add(&mut self, params: Vec<Eval<LispNumber>>) -> Result<Atom> {
        let mut sum = 0.;
        for v in params {
            sum += v.unwrap().0
        }
        Ok(sum.into())
    }

    fn sub(&mut self, params: Vec<Eval<LispNumber>>) -> Result<Atom> {
        let mut sum = 0.;
        for v in params {
            sum -= v.unwrap().0
        }
        Ok(sum.into())
    }

    fn mul(&mut self, params: Vec<Eval<LispNumber>>) -> Result<Atom> {
        let mut sum = 1.;
        for v in params {
            sum *= v.unwrap().0
        }
        Ok(sum.into())
    }

    fn div(&mut self, (first, items): (Eval<LispNumber>, Vec<Eval<LispNumber>>)) -> Result<Atom> {
        let mut current = first.0;
        if items.is_empty() {
            current = 1.0 / current;
        }
        for second in items {
            let value = second.0;
            if value == 0.0 {
                return Err(LispError::DivisionByZero {
                    first: current,
                    second: value,
                });
            }
            current /= value;
        }
        Ok(current.into())
    }

    fn modulus(&mut self, (first, second): (Eval<LispNumber>, Eval<LispNumber>)) -> Result<Atom> {
        if second.0 == 0.0 {
            Err(LispError::DivisionByZero {
                first: first.0,
                second: second.0,
            })
        } else {
            Ok((first.0 % second.0).into())
        }
    }
}
