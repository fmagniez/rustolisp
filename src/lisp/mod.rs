use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::ops::{Deref, DerefMut};
use std::sync::Arc;

pub use atoms::*;
pub use error::LispError;

mod language;
mod functions;
mod error;
mod atoms;

type Result<T> = std::result::Result<T, LispError>;

pub struct EvalContext {
    symbols: HashMap<String, Atom>,
}

impl EvalContext {
    pub fn new() -> Self {
        Self {
            symbols: Default::default(),
        }
    }

    fn get(&mut self, name: &LispSymbol) -> Result<Atom> {
        if "NIL" == name.0 || "T" == name.0 {
            return Ok(name.clone().into());
        }
        self.symbols
            .get(name.0.as_str())
            .cloned()
            .ok_or_else(|| LispError::VariableNotInScope {
                symbol: name.clone()
            })
    }

    fn set(&mut self, name: &LispSymbol, value: Atom) -> Result<()> {
        if "NIL" == name.0 || "T" == name.0 {
            return Err(LispError::LanguageDefinedSymbol { symbol: name.clone() });
        }
        self.symbols.insert(name.0.clone(), value);
        Ok(())
    }
}

pub trait Expr: Display + Clone + Into<Atom> {
    fn eval(&self, context: &mut EvalContext) -> Result<Atom>;
    fn eval_map(&self, context: &mut EvalContext) -> Result<Atom> {
        self.eval(context)
            .map_err(|e| LispError::EvalError {
                atom: self.clone().into(),
                err: Box::new(e),
            })
    }

    fn is_nil(&self) -> bool {
        false
    }

    fn as_symbol(&self) -> Result<&LispSymbol> {
        Err(LispError::NotASymbol { value: self.clone().into() })
    }
    fn as_number(&self) -> Result<&LispNumber> {
        Err(LispError::NotANumber { value: self.clone().into() })
    }
    fn as_string(&self) -> Result<&LispString> {
        Err(LispError::NotAString { value: self.clone().into() })
    }
    fn as_list(&self) -> Result<&LispList> {
        Err(LispError::NotAList { value: self.clone().into() })
    }
}
