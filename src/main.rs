use rustyline::{Editor, error::ReadlineError};

use rustolisp::{EvalContext, Expr};
use rustolisp::parser::{LispParseError, parse_lisp};

fn main() -> rustyline::Result<()> {
    let mut rl = Editor::<()>::new();
    let mut context = EvalContext::new();
    loop {
        let line = rl.readline("\x1B[37m>\x1B[0m ");
        if let
        Err(ReadlineError::Eof) |
        Err(ReadlineError::Interrupted) = line {
            break;
        }
        let mut line = line?;
        {
            let line = line.trim();
            if line.is_empty() || line.starts_with(';') {
                continue;
            }
        }
        let mut parsed = parse_lisp(line.as_str());
        let mut multiline = false;
        while let Err(LispParseError::MissingClosingParenthesis { depth, .. }) = parsed {
            multiline = true;
            let newline = rl.readline(format!("\x1B[37m.{} \x1B[0m ", "..".repeat(depth)).as_str());
            if let
            Err(ReadlineError::Eof) |
            Err(ReadlineError::Interrupted) = newline {
                break;
            }
            let newline = newline?;
            if newline.is_empty() {
                continue;
            }
            line = format!("{}\n{}", line, newline);
            parsed = parse_lisp(line.as_str())
        }
        rl.add_history_entry(line);
        match parsed {
            Err(err) => eprintln!("{}ERREUR DE SYNTAXE: {}\x1B[0m", if multiline {
                format!("\x1B[1;31m")
            } else {
                format!("  \x1B[1;31m{}^\n", " ".repeat(err.position()))
            }, err),
            Ok(expr) => match expr.eval_map(&mut context) {
                Err(err) => eprintln!("\x1B[1;31mERREUR: {}\x1B[0m", err),
                Ok(result) => println!("{}", result)
            }
        }
    }
    Ok(())
}

/*
23
(quote 23)
'23
(set x 32)
(setq x 32)
(list x x 32)
(cadr (list x x 32))
(setq x 'y)
(setq xx 5)
(setq y '(+ xx xx 32))
x
(eval x)
(eval y)
(cadr y)
(eval (list '+ (eval y)(cadr y)))
(setq z (+ (if x 2 0) (cadddr y) (* y y)))
(setq y (list (setq z "Albert") x y))
z
y
(setq x (* x x))
x

assert_parse_eval("23", "23");
assert_parse_eval("(quote 23)", "23");
assert_parse_eval("'23", "23");
assert_parse_eval_err("(set x 32)", "ERROR: no variable named X in scope\n  at function SET, parameter 1");
assert_parse_eval("(setq x 32)", "32");
assert_parse_eval("(list x x 32)", "(32 32 32)");
assert_parse_eval("(cadr (list x x 32))", "32");
assert_parse_eval("(setq x 'y)", "Y");
assert_parse_eval("(setq xx 5)", "5");
assert_parse_eval("(setq y '(+ xx xx 32))", "(+ XX XX 32)");
assert_parse_eval("x", "Y");
assert_parse_eval("(eval x)", "(+ XX XX 32)");
assert_parse_eval("(eval y)", "42");
assert_parse_eval("(cadr y)", "XX");
assert_parse_eval("(eval (list '+ (eval y)(cadr y)))", "47");
assert_parse_eval_err("(setq z (+ (if x 2 0) (caddr y) (* y y)))", "ERROR: expected a number but got XX\n  at function +, parameter 2\n  at function SETQ, parameter 2");
assert_parse_eval("(setq y (list (setq z "Albert") x y))", "("Albert" Y (+ XX XX 32))");
assert_parse_eval("z", ""Albert"");
assert_parse_eval("y", "("Albert" Y (+ XX XX 32))");
assert_parse_eval_err("(setq x (* x x))", "ERROR: no function named * in scope\n  at function SETQ, parameter 2");
assert_parse_eval("x", "Y");


 */