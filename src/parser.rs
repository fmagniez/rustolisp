use crate::lisp::Atom;

#[derive(thiserror::Error, Debug)]
pub enum LispParseError {
    #[error("Il manque quelque chose à la fin de l'expression")]
    UnexpectedEOF { position: usize },
    #[error("Il manque un Symbole après une Quote (')")]
    MissingSymbol { position: usize },
    #[error("{} listes non fermée(s)", depth)]
    MissingClosingParenthesis { position: usize, depth: usize },
    #[error("Ceci n'est pas un Nombre valide: \x1B[0;1;36m{}\x1B[31m", number)]
    MalformedNumber { position: usize, number: String },
    #[error("Une seule expression attendue; certains caractères sont en trop")]
    ExtraneousCharacters { position: usize },
}

impl LispParseError {
    pub fn position(&self) -> usize {
        match self {
            LispParseError::UnexpectedEOF { position, .. } |
            LispParseError::MissingSymbol { position, .. } |
            LispParseError::MissingClosingParenthesis { position, .. } |
            LispParseError::MalformedNumber { position, .. } |
            LispParseError::ExtraneousCharacters { position, .. } => *position,
        }
    }

    fn position_mut(&mut self) -> &mut usize {
        match self {
            LispParseError::UnexpectedEOF { position, .. } |
            LispParseError::MissingSymbol { position, .. } |
            LispParseError::MissingClosingParenthesis { position, .. } |
            LispParseError::MalformedNumber { position, .. } |
            LispParseError::ExtraneousCharacters { position, .. } => position,
        }
    }
}

pub fn parse_lisp(input: &str) -> Result<Atom, LispParseError> {
    fn parse_lisp(input: &mut &str, depth: usize) -> Result<Atom, LispParseError> {
        *input = input.trim();
        let mut first: Option<char> = input.chars().nth(0);
        if let Some(';') = first {
            match input.split_once('\n') {
                None => first = None,
                Some((_, left)) => {
                    *input = left.trim();
                    first = input.chars().nth(0);
                }
            }
        }
        match first {
            None => Err(LispParseError::UnexpectedEOF {
                position: 0
            }),

            Some('\'') => {
                *input = &input[1..].trim();
                Ok(parse_lisp(input, depth)?.quote())
            }
            Some('"') => {
                *input = &input[1..].trim();
                let pos = input
                    .chars()
                    .position(|c| c == '"')
                    .unwrap_or(input.len());
                let (value, left) = input.split_at(pos);
                *input = &left[1..].trim();
                Ok(Atom::string(value))
            }
            Some('(') => {
                *input = &input[1..].trim();
                let mut items = vec![];
                while let Some(v) = input.chars().nth(0) {
                    let v = if v == ';' {
                        match input.split_once('\n') {
                            None => break,
                            Some((_, left)) => {
                                *input = left.trim();
                                if let Some(v) = input.chars().nth(0) {
                                    v
                                } else {
                                    break;
                                }
                            }
                        }
                    } else {
                        v
                    };
                    if v == ')' {
                        break;
                    }
                    items.push(parse_lisp(input, depth + 1)?);
                }
                *input = input
                    .strip_prefix(')')
                    .ok_or_else(|| LispParseError::MissingClosingParenthesis {
                        position: 0,
                        depth,
                    })?
                    .trim();
                Ok(items.into())
            }

            Some(_) => {
                let pos = input
                    .chars()
                    .position(|c| c == ')' || c.is_whitespace())
                    .unwrap_or(input.len());
                let (value, left) = input.split_at(pos);
                if value.is_empty() {
                    return Err(LispParseError::MissingSymbol {
                        position: 0
                    })
                }
                *input = left.trim();
                match value.parse::<f64>() {
                    Ok(nb) if nb.is_finite() => Ok(nb.into()),
                    _ => Ok(value.into()),
                }
            }
        }
    }
    let mut left = input;
    parse_lisp(&mut left, 1)
        .and_then(|result| if left.is_empty() || left.chars().nth(0).unwrap() == ';' {
            Ok(result)
        } else {
            Err(LispParseError::ExtraneousCharacters {
                position: input.len() - left.len()
            })
        })
        .map_err(|mut e| {
            let pos = input.len() - left.len();
            *e.position_mut() = pos;
            e
        })
}
