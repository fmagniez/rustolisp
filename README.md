# Rust'o'LISP

> Un joli interpréteur LISP en Rust (joli = avec une interface en couleur et en français)

<center>
<img alt="Demonstration" src="./doc/showcase.png" title="Demonstration"/>
</center>

## Fonctionalités

 * En couleurs : coloration syntaxique des listes
 * En Français : messages d'erreur détaillés écrits en Français
 * En Rust, en Open Source : le code de ce projet était disponible en Open Source, il permet à n'importe qui de l'initier au langage Rust
 * Simple : un seul exécutable sans dépendances, aucune configuration nécessaire

## Support

Le LISP est un ancien langage qui a beaucoup évolué au fils du temps,
et propose donc aujourd'hui une large gamme de fonctionalités.
De ce fait, ce projet demandera pas mal d'efforts pour être
le plus compatible possible. Votre contribution est le bienvenue !

Actuellement, les fonctionnalités suivantes sont prévues (celles cochées fonctionnent déjà):
 * [X] REPL (Read-Eval-Print-Loop)
 * [X] Report des erreurs (syntaxiques et d'évaluation)
 * [X] Symboles
 * [X] Nombres
 * [X] Chaines de caractères
 * [X] Listes
 * [X] Quotes (`'`, `QUOTE`))
 * [X] Variables (`SET`, `SETQ`)
 * [X] Opérateurs mathématiques (`+`, `-`, `*`, `/`, `MOD`)
 * [ ] Opérateurs logiques (`<`, `<=`, `=`, `>=`, `>`, etc.)
 * [X] Opérateur conditionnel (`IF`)
 * [ ] Boucles (`LOOP`)
 * [ ] Fonctions (`DEFUN`)
 * [ ] Lambda (`LAMBDA`)

## Utilisation

 1. Installer [Rust](https://rust-lang.org/)
 2. Récupérez le projet
 3. Tapez dans le dossier du projet la commande : `cargo run`
 4. C'est tout !
    Si tout s'est bien passé, vous devriez avoir obtenu un *prompt*: `>`.
    Vous pouvez commencer à entrer des expressions LISP à évaluer.

Sous Windows, il est possible que la coloration syntaxique ne fonctionne pas correctement avec certains terminaux comme `cmd`.
Essayez avec `powershell` si cela pose problème.
Tout autre système compatible Posix (Linux, MacOS, etc.).

## Contribution

 * Comment proposer une modification:
   1. Faire un fork de ce projet
   2. Ajouter vos modifications
   3. Faire une demande de fusion (Merge Request)
   4. Laisser une personne autorisée accepter ou refuser votre Merge Request
 * Bien que la **documentation** et les **messages d'erreur** soient en **français**, le **code** et les **messages de commits** sont en **anglais**.
 * Pensez à lancer `cargo test` pour vérifier que la plupart des fonctionalités fonctionnent toujours

## TODO

* [ ] Améliorer la documentation pour d'éventuels future contributeurs (ouvrez une issue ou envoyez un mail pour toute question en attendant)
* [ ] Ajouter les fonctionnalités manquantes décrites précedement
* [ ] Optimiser le code (surtout simplifier les `Arc<...>`)
* [ ] Système de traduction en utilisant les `#[feature(...)]` de Rust ?
* [ ] Ajout d'une intégration continue (CI)
* [ ] Span (mise en évidence dans le texte d'entrée la partie qui a causé une erreur)
* [ ] Language Server (pour la validation du code LISP dans les éditeur de code)

## FAQ

### Pourquoi avoir fait un nouvel interpréteur LISP ? Il y en a déjà plein

1. Pour améliorer mes connaissances et compétences en Rust
2. Pour améliorer mes connaissances et compétences en LISP (pour IA01)
3. Pour mettre en pratique NF11
4. Pour (je l'espère) aider et inspirer d'autres personnes intéressées

<details>
Effectivement il y en a déjà plein, mais je les trouvent compliquées,
surtout leur intégration aux IDEs (peu de plugins ou de mauvaise qualité).
C'est en constatant cela lorsque je commençais IA01 que je me suis dit que,
vu la certaine "simplicité" du langage (contrairement au C par exemple),
il pourrait être intéressant de refaire toute l'analyse et l'évaluation
du langage depuis zéro, afin ne serait-ce que de vérifier ma compréhension
du LISP ou pour mettre en pratique NF11.

J'ai donc commencé ce projet le Mercredi 15 Septembre vers 21h, et à ma grande
surprise la plupart des fonctionalités et l'interface étaient déjà bien avancées
24h plus tard, ce qui m'a incité à mettre ce projet sur le Gitlab de l'UTC, afin
qu'il profite aux intéressées.
</details>

### L'analyse du texte en entrée ne semble pas correspondre à ce que l'on voit dans l'UV NF11 (pas de lexer et parser, juste une fonction). Pourquoi ?

Le langage LISP étant très facile à analyser programmatiquement, il n'est
pas nécessaire de séparer ces 2 étapes. En fait, à part les parenthèses
et les quotes (`'`), les jetons générés par le lexers seraient directement
les feuilles de l'arbre syntaxique abstrait (AST); il est donc tout aussi simple
de prendre comme jetons d'entrée directement les caractères de la chaine d'entrée.
